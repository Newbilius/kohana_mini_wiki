<?php defined('SYSPATH') or die('No direct script access.');

require SYSPATH.'classes/Kohana/Core'.EXT;
//require APPPATH.'classes/Kohana'.EXT;

if (is_file(APPPATH.'classes/kohana'.EXT))
{
	// Application extends the core
	require APPPATH.'classes/kohana'.EXT;
}
else
{
	// Load empty core extension
	require SYSPATH.'classes/kohana'.EXT;
}

date_default_timezone_set('America/Chicago');
//date_default_timezone_set('Asia/Yekaterinburg'); //тот ли пояс
setlocale(LC_ALL, 'en_US.utf-8'); //локаль

spl_autoload_register(array('Kohana', 'auto_load'));
//ini_set('unserialize_callback_func', 'spl_autoload_call');

I18n::lang('ru-ru'); //язык

if (isset($_SERVER['KOHANA_ENV']))
{
	Kohana::$environment = constant('Kohana::'.strtoupper($_SERVER['KOHANA_ENV']));
}

/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - integer  cache_life  lifetime, in seconds, of items cached              60
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 * - boolean  expose      set the X-Powered-By header                        FALSE
 */
Kohana::init(array(
	'base_url'   => '/',
        'index_file' =>'',
));

Cookie::$salt = "jhtyfg655y5ef5gygvau7ke6_";

Kohana::$log->attach(new Log_File(APPPATH.'logs'));

Kohana::$config->attach(new Config_File);

Kohana::modules(array(
         'pagination'   => MODPATH.'pagination',   // Database access
	 'database'   => MODPATH.'database',   // Database access
	 'orm'        => MODPATH.'orm',        // Object Relationship Mapping
	));

Route::set('journal','journal(/<page>)')
	->defaults(array(
		'controller' => 'journal',
		'action'     => 'index',
                'page'       => 1,
	));
Route::set('create','create')
	->defaults(array(
		'controller' => 'wiki',
		'action'     => 'create',
	));
Route::set('default', '(<word>(/<action>))', array('action' => '(view|edit|delete|add|index|create)'))
	->defaults(array(
		'controller' => 'wiki',
		'action'     => 'index',
                'word'       => '',
	));