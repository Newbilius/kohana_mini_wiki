<?php

defined('SYSPATH') or die('No direct script access.');

class Layout {

    public static function error($errors) {
        echo View::factory("layout/errors")->Set("errors",$errors);
    }

}