<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Journal extends ORM {

    protected $_created_column = array(
        'column' => 'datetime',
        'format' => 'Y-m-d H:i:s'
    );
    protected $_table_name = "journal";

}