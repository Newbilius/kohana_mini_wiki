<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Word extends ORM {

    protected $_table_name = "words";
    protected $old_data=null;
            
    protected function _load_values(array $values) {
        $result=parent::_load_values($values);
        
        if ($this->_loaded) {
            if ($this->old_data === null) {
                $this->old_data = clone $this;
            }
        };

        return $result;
    }

    public function save(Validation $validation = NULL) {
        $operation = "add";

        if ($this->old_data!==null) {
            $operation = "edit";
        };

        $result = parent::save();

        if ($result) {
            $journal_item = ORM::factory("Journal");
            $journal_item->text_after = $this->text;
            if ($operation=="edit")
                $journal_item->text_before = $this->old_data->text;
            $journal_item->name = $this->name;
            $journal_item->operation = $operation;
            $journal_item->save();
        }

        return $result;
    }

    public function delete() {
        if ($this->loaded()) {
            $journal_item = ORM::factory("Journal");
            $journal_item->text_after = $this->text;
            $journal_item->name = $this->name;
            $journal_item->operation = "delete";
            $journal_item->save();
        };
        return parent::delete();
    }

}