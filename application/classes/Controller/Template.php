<?php

defined('SYSPATH') OR die('No direct script access.');

abstract class Controller_Template extends Kohana_Controller_Template {

    protected $content;
    public $template_file = "";
    public $template = "template";
    public $active_menu = "";
    public $title = "";
    public $menu_array = Array(
        "index" => Array("caption" => "Все слова", "url" => "/"),
        "journal" => Array("caption" => "Журнал учета операций", "url" => "/journal"),
        "create" => Array("caption" => "Добавить слово", "url" => "/create"),
        "about" => Array("caption" => "Об авторе :)", "url" => "http://www.siteszone.ru"),
    );

    protected function set($name,$value){
        $this->content->$name=$value;
    }
    
    public function before() {
        parent::before();
        if ($this->template_file) {
            $this->content = View::factory($this->template_file);
        }
    }

    public function after() {
        $this->template->_content = $this->content->render();
        $this->template->active_menu=$this->active_menu;
        $this->template->menu_array=$this->menu_array;
        $this->template->title=$this->title;
        parent::after();
    }

}