<?php

defined('SYSPATH') or die('No direct script access.');

//@todo верный способ Route::url('admin', array('controller' => 'eventypes'));

class Controller_Wiki extends Controller_Template {

    public $title="Мини-вики";
    public $template_file = "wiki/start";
    public $active_menu = "";
    public $word;
    public $word_obj;

    public function clear($data){
        return strip_tags($data,"<p><b><strong><i><u>");
    }
    
    public function action_create() {
        $this->active_menu = "create";
        if (isset($_GET["name"])) {
            if ($_GET["name"]) {
                $word = $_GET["name"];
                $this->redirect("/{$word}/add");
            } else {
                $this->set("errors", "необходимо ввести название слова");
            }
        }
        $this->content->set_filename("wiki/create");
    }

    public function action_index() {
        $this->active_menu = "index";
        $this->content->list = ORM::factory("word")->order_by("name", "ASC")->find_all();
    }

    public function before() {
        parent::before();
        $word = $this->request->param('word');

        $this->word = ORM::factory("word")->where("name", "=", $word)->find();
        if ($word == "index") {
            $this->redirect("/");
        }

        if ($word != "" && $word != "index") {
            if (!$this->word->loaded()) {
                $this->word->name = $word;
                if ($this->request->action() == "view") {
                    $this->redirect("/{$word}/add");
                }
            };
            if ($this->word->loaded()) {
                if ($this->request->action() == "add") {
                    $this->redirect("/{$word}/edit");
                }
                if ($this->request->action() == "index") {
                    $this->redirect("/{$word}/view");
                }
            }
        }
    }

    public function action_view() {
        $this->content->set_filename("wiki/view");
        $this->content->word = $this->word->name;
        $this->content->text = $this->word->text;
    }

    public function action_delete() {
        if ($this->word->loaded()) {
            $this->word->delete();
        }
        $this->redirect("/");
    }

    public function action_add() {
        $this->content->set_filename("wiki/word_edit");
        $this->content->caption = "Добавляем слово";
        $this->content->word = $this->clear($this->word->name);

        if ($this->request->method() == Request::POST) {
            //я в курсе про класс вадидации, но использовать его из-за одного поля?
            //Из пушки по воробьям...
            if ($this->request->post("text")) {
                $this->word->text = $this->clear($this->request->post("text"));
                $this->word->save();
                $this->redirect("/" . $this->word->name . "/view");
            } else {
                $this->set("errors", "не введен текст.");
            }
        }
    }

    public function action_edit() {
        $this->content->set_filename("wiki/word_edit");
        $this->content->caption = "Редактируем слово";
        $this->content->word = $this->clear($this->word->name);
        if ($this->request->method() == Request::POST) {
            if ($this->request->post("text")) {
                $this->word->text = $this->clear($this->request->post("text"));
                $this->word->save();
                $this->redirect("/" . $this->word->name . "/view");
            } else {
                $this->set("errors", "не введен текст.");
            }
        }
        $this->content->text = $this->word->text;
    }

}