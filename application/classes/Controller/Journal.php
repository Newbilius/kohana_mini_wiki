<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Journal extends Controller_Template {

    public $title = "Журнал учета операций";
    public $template_file = "journal";
    public $active_menu = "journal";
    protected $count_on_page = 5;

    public function action_index() {
        $page = $this->request->param('page');
        $page = intval($page);
        if ($page < 1) {
            $page = 1;
        }

        $list_src = ORM::factory("journal");

        $pagination = Pagination::factory(array(
                    'current_page' => array('source' => 'route', 'key' => 'page'),
                    'total_items' => $list_src->count_all(),
                    'items_per_page' => $this->count_on_page,
                ))->route_params(array(
                    'controller' => Request::current()->controller(),
                    'action' => Request::current()->action(),
                ));

        $list = $list_src
                ->offset($pagination->offset)
                ->limit($this->count_on_page)
                ->order_by("datetime", "DESC")
                ->find_all();
        $this->content->list = $list;
        $this->content->paginator=$pagination;
    }

}