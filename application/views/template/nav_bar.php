<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="brand" href="/">Тестовое задание - мини-вики</a>
            <div class="nav-collapse collapse">
                <ul class="nav">
                    <?
                    echo View::factory("template/menu")
                            ->Set("active_menu", $active_menu)
                            ->Set("menu_array", $menu_array);
                    ?>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>
</div>