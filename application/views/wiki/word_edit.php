<?
if (isset($errors)) {
    Layout::error($errors);
}
?>
<form class="form-horizontal" method="post">
    <div class="control-group">
        <label class="control-label"><?= $caption ?></label>
        <div class="controls">
            <span><?= $word ?></span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="text">Текст</label>
        <div class="controls">
            <textarea id="text" name="text" ><? if (isset($text)) echo $text ?></textarea>
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <input type="submit" class="btn btn-success" value="сохранить">
        </div>
    </div>
</form>