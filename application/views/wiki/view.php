<p>Слово <b><?= $word ?></b></p>

<?php
if (isset($text))
    echo $text;
?>

<p><br>
    <a class="btn btn-success" href="/<?= $word ?>/edit"><i class="icon-pencil icon-white"></i> Отредактировать</a>
    <a class="btn btn-danger danger_link" alt="вы уверены, что хотите удалить слово?" href="/<?= $word ?>/delete"><i class="icon-trash icon-white"></i> Удалить</a>
    <a class="btn btn-primary" href="/"><i class="icon-arrow-left icon-white"></i> Вернуться к списку</a>
</p>