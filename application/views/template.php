<!DOCTYPE html>
<html>
    <head>
        <title><?=$title?></title>
        <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <script src="/bootstrap/js/jquery.js"></script>
        <script src="/bootstrap/js/bootstrap.min.js"></script>
        
        <link rel="stylesheet" href="/redactor/redactor.css" />
        <script src="/redactor/redactor.min.js"></script>
        
        <link rel="stylesheet" href="/design/wiki.css" />
        <script src="/design/wiki.js"></script>
    </head>
    <body>
        <?
        echo View::factory("template/nav_bar")
                ->Set("active_menu", $active_menu)
                ->Set("menu_array", $menu_array)
        ;
        ?>
        <div class="container">
            <? if (isset($_content)) echo $_content ?>

        </div> <!-- /container -->

    </body>
</html>