<?
$caption_array = Array("id", "Слово", "Действие", "Дата и время", "Текст ДО", "Текст ПОСЛЕ");
$fields_array = Array("id", "name", "operation", "datetime", "text_before", "text_after");
?>

<table class="table table-bordered table-condensed table-striped" width="100%">
    <thead>
        <tr>
            <?
            foreach ($caption_array as $_caption) {
                ?>
                <th>
                    <?= $_caption ?>
                </th>
                <?
            }
            ?>
        </tr>
    </thead>
    <?
    if (isset($list)) {
        foreach ($list as $_item) {
            ?>
            <tr>
                <?
                foreach ($fields_array as $_field) {
                    ?>
                    <td>
                        <?
                        if ($_field == "operation") {
                            echo __($_item->$_field);
                        } else {
                            echo $_item->$_field;
                        };
                        ?>
                        &nbsp;
                    </td>
                    <?
                }
                ?>
            </tr>
            <?
        }
    }
    ?>
</table>

<? echo $paginator; ?>