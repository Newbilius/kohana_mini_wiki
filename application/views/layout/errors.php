<?
if (isset($errors)) {
    ?>
    <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <?
        if (is_array($errors)) {
            foreach ($errors as $_error) {
                echo $_error . "<BR>";
            }
        } else {
            echo $errors;
        }
        ?>
    </div>
    <?
}
?>