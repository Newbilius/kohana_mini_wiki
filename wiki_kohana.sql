-- phpMyAdmin SQL Dump
-- version 3.2.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 14, 2013 at 08:59 PM
-- Server version: 5.1.40
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wiki_kohana`
--

-- --------------------------------------------------------

--
-- Table structure for table `journal`
--

CREATE TABLE IF NOT EXISTS `journal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext NOT NULL,
  `operation` enum('add','edit','delete') NOT NULL,
  `datetime` datetime NOT NULL,
  `text_before` text NOT NULL,
  `text_after` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `journal`
--

INSERT INTO `journal` (`id`, `name`, `operation`, `datetime`, `text_before`, `text_after`) VALUES
(1, 'Кукарача', 'add', '2013-02-14 10:52:28', '', '<p>Таракан? Что ли...</p>'),
(2, 'Кукарача', 'edit', '2013-02-14 10:54:39', '<p>Таракан? Что ли...</p>', '<p>Так то ж бубльгум! Песня такая. Наверняка.</p>\n'),
(3, 'дополнительное слово', 'add', '2013-02-14 10:55:09', '', '<p>без текста слова не добавиииить.</p><p></p>'),
(4, 'дополнительное слово', 'edit', '2013-02-14 10:55:48', '<p>без текста слова не добавиииить.</p><p></p>', '<p>без текста слова не д<strong>обавиииить.1111111</strong>1111111111</p><p></p>\n'),
(5, 'дополнительное слово', 'edit', '2013-02-14 10:57:04', '<p>без текста слова не д<strong>обавиииить.1111111</strong>1111111111</p><p></p>\n', '<p>без текста сло</p><p></p>\n'),
(6, 'дополнительное слово', 'edit', '2013-02-14 10:57:11', '<p>без текста сло</p><p></p>\n', '<p>без текста. хы.</p><p></p>\n'),
(7, 'дополнительное слово', 'edit', '2013-02-14 10:57:58', '<p>без текста. хы.</p><p></p>\n', '<p>без текста. хы.</p><p></p>\n'),
(8, 'дополнительное слово', 'delete', '2013-02-14 10:58:08', '', '<p>без текста. хы.</p><p></p>\n'),
(9, 'Doom', 'add', '2013-02-14 10:58:22', '', 'Классика жанра.'),
(10, 'Doom', 'edit', '2013-02-14 10:58:39', 'Классика жанра.', 'Классика <strong>жанра.</strong>\n'),
(11, 'Банан', 'add', '2013-02-14 10:59:07', '', '<p>Фруууктик.</p><p>Определенно.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `words`
--

CREATE TABLE IF NOT EXISTS `words` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext CHARACTER SET utf8 NOT NULL,
  `text` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `words`
--

INSERT INTO `words` (`id`, `name`, `text`) VALUES
(1, 'Кукарача', '<p>Так то ж бубльгум! Песня такая. Наверняка.</p>\n'),
(3, 'Doom', 'Классика <strong>жанра.</strong>\n'),
(4, 'Банан', '<p>Фруууктик.</p><p>Определенно.</p>');
